<?php

declare(strict_types=1);

namespace UI\Api\Category\Action;

use Infrastructure\Repository\Doctrine\Category\CategoryRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use Core\Category\Domain\Entity\Category;

final class ListCategoryController extends AbstractFOSRestController
{
    private CategoryRepository $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * List all categories.
     *
     * @Route("/api/categories", name="api_category_list", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns all categories",
     *     @Model(type=Category::class)
     * )
     */
    public function __invoke(): Response
    {
        $categories = $this->categoryRepository->findAll();
        $view = $this->view($categories, 200)->setFormat('json');

        return $this->handleView($view);
    }
}