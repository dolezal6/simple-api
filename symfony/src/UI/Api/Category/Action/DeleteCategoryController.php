<?php

declare(strict_types=1);

namespace UI\Api\Category\Action;

use Core\Category\Domain\Entity\Category;
use Infrastructure\Repository\Doctrine\Category\CategoryRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

final class DeleteCategoryController extends AbstractFOSRestController
{
    private CategoryRepository $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Delete category.
     *
     * @Route("/api/category/{id}/delete", name="api_category_delete", methods={"DELETE"})
     * @OA\Response(
     *     response=200,
     *     description="Deletes category",
     *     @Model(type=Category::class)
     * )
     */
    public function __invoke(Category $category): Response
    {
        $this->categoryRepository->remove($category);

        return $this->handleView(
            $this->view([
                'status' => 'Category deleted',
            ],
                Response::HTTP_OK
            )
                ->setFormat('json')
        );
    }
}