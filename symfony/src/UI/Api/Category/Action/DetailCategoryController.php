<?php

declare(strict_types=1);

namespace UI\Api\Category\Action;

use Core\Category\Domain\Entity\Category;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

final class DetailCategoryController extends AbstractFOSRestController
{
    /**
     * Show category detail.
     *
     * @Route("/api/category/{id}", name="api_category_detail", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="Returns category detail",
     *     @Model(type=Category::class)
     * )
     */
    public function __invoke(Category $category): Response
    {
        $view = $this->view($category, 200)->setFormat('json');

        return $this->handleView($view);
    }
}
