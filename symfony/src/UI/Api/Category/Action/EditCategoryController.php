<?php

declare(strict_types=1);

namespace UI\Api\Category\Action;

use Core\Category\Domain\Entity\Category;
use Infrastructure\Repository\Doctrine\Category\CategoryRepository;
use UI\Api\Category\Mapping\DTO\CategoryType;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

final class EditCategoryController extends AbstractFOSRestController
{
    private CategoryRepository $categoryRepository;

    public function __construct(
        CategoryRepository $categoryRepository
    )
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Edit category.
     *
     * @Route("/api/category/{id}/edit", name="api_actegory_edit", methods={"PUT"})
     * @OA\Response(
     *     response=200,
     *     description="Edits category",
     *     @Model(type=Category::class)
     * )
     * @OA\Parameter(
     *     name="title",
     *     in="query",
     *     description="New category name",
     *     @OA\Schema(type="string")
     * )
     */
    public function __invoke(Request $request, Category $category): Response
    {
        $form = $this->createForm(CategoryType::class, $category);

        $form->submit($request->toArray());

        if ($form->isValid()) {
            $this->categoryRepository->save($category);

            return $this->handleView(
                $this->view([
                    'status' => 'Category edited',
                ],
                    Response::HTTP_OK
                )
                    ->setFormat('json')
            );
        }

        throw new HttpException(
            422,
            'Invalid JSON'
        );
    }
}