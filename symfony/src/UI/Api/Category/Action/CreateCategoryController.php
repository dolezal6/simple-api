<?php

declare(strict_types=1);

namespace UI\Api\Category\Action;

use Core\Category\Application\Repository\CategoryRepositoryInterface;
use Core\Category\Domain\Entity\Category;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use UI\Api\Category\Mapping\DTO\CategoryType;

final class CreateCategoryController extends AbstractFOSRestController
{
    private CategoryRepositoryInterface $categoryRepository;

    public function __construct(
        CategoryRepositoryInterface $categoryRepository
    ) {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Create new category.
     *
     * @Route("/api/category/create", name="api_category_create", methods={"POST"})
     * @OA\Response(
     *     response=200,
     *     description="Creates new category",
     *     @Model(type=Category::class)
     * )
     * @OA\Parameter(
     *     name="title",
     *     in="query",
     *     description="The name of the new category",
     *     @OA\Schema(type="string")
     * )
     */
    public function __invoke(Request $request): Response
    {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);

        $form->submit($request->query->all());

        if (false === $form->isValid()) {
            throw new HttpException(
                422,
                'Invalid JSON'
            );
        }

        $this->categoryRepository->save($category);

        return $this->handleView(
            $this->view(['status' => 'Category created'], Response::HTTP_CREATED)->setFormat('json')
        );
    }
}
