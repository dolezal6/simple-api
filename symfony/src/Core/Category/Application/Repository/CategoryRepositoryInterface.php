<?php

declare(strict_types=1);

namespace Core\Category\Application\Repository;

use Core\Category\Domain\Entity\Category;

interface CategoryRepositoryInterface
{
    public function save(Category $category): void;

    public function remove(Category$category): void;
}